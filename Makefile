all: man-of-sorrows-melody.pdf \
	jesus-loves-me-melody.pdf \
	therefore-i-urge-you.pdf

%.ps: %.mup
	mup $< > $@

%.pdf: %.ps
	pstopdf $< -o $@
